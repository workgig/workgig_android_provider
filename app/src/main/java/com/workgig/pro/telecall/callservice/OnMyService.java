package com.workgig.pro.telecall.callservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.workgig.pro.mqtt.MqttEvents;
import com.workgig.pro.mqtt.MqttHelper;
import com.workgig.pro.telecall.UtilityVideoCall;
import com.workgig.pro.utility.AppController;
import com.workgig.pro.utility.SessionManager;
import com.workgig.pro.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;


public class OnMyService extends Service {
    SessionManager sharedPrefs;

    MqttHelper manager;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        sharedPrefs = SessionManager.getSessionManager(this);
        manager=AppController.getInstance().getMqttHelper();
        Log.e("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utility.printLog("ClearFromRecentService", "Service Destroyed");
        JSONObject obj = new JSONObject();
        try {
            obj.put("status", 1);
            manager.publish(MqttEvents.CallsAvailability.value + "/" + sharedPrefs.getProviderId(), obj, 0, true);//UserId
            UtilityVideoCall.getInstance().setActiveOnACall(false, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        try {
            String cid = sharedPrefs.getProviderId();
            if(manager!=null)
            {
             manager=AppController.getInstance().getMqttHelper();
            }

            Log.e("ClearFromRecentService", "END " + cid + " MQTTRESPO " + manager.isMqttConnected());
            //Code here

            if (manager.isMqttConnected()) {
                if (!cid.equals("")) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("status", 1);
                        manager.publish(MqttEvents.CallsAvailability.value + "/" + sharedPrefs.getProviderId(), obj, 0, true);//UserId
                        UtilityVideoCall.getInstance().setActiveOnACall(false, false);
                        //    manager.subscribeToTopic(MqttEvents.Calls.value+"/"+sharedPrefs.getSID(),1);
                        //  mqttManager.subscribeToTopic(MqttEvents.Calls.value+"/5c177bf2f56745d4b143e1a6",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                 /*   manager.unsubscribeToTopic(MqttEvents.JobStatus.value + "/" +cid);
                    manager.unsubscribeToTopic(MqttEvents.Provider.value + "/" +cid);
                    manager.unsubscribeToTopic(MqttEvents.Message.value + "/" + cid);
                    //  manager.publish(MqttEvents.CallsAvailability.value+"/"+cid);
                    if(!VariableConstant.LiveTrackBookingPid.equals(""))
                        manager.unsubscribeToTopic(MqttEvents.LiveTrack.value + "/" + VariableConstant.LiveTrackBookingPid);
                    VariableConstant.LiveTrackBookingPid = "";*/
                    manager.disconnect(/*cid*/);
                }
            } else {
                if (!cid.equals("")) {
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("status", 1);
                        manager.publish(MqttEvents.CallsAvailability.value + "/" + sharedPrefs.getProviderId(), obj, 0, true);//UserId
                        UtilityVideoCall.getInstance().setActiveOnACall(false, false);
                        //    manager.subscribeToTopic(MqttEvents.Calls.value+"/"+sharedPrefs.getSID(),1);
                        //  mqttManager.subscribeToTopic(MqttEvents.Calls.value+"/5c177bf2f56745d4b143e1a6",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            /*if(!sharedPrefs.getGuestLogin())
            {
                sharedPrefs.setAppOpen(false);
            }
*/
            stopSelf();
        } catch (Exception a) {
            a.printStackTrace();
        }
    }

}
