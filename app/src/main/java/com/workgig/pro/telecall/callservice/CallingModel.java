package com.workgig.pro.telecall.callservice;

import com.google.gson.Gson;
import com.workgig.pro.pojo.callpojo.PostCallResponse;

public class CallingModel {

    private Gson gson;

    public CallingModel() {
        gson = new Gson();
    }

    public String parseCallerId(String response) {
        try {
            PostCallResponse postCallResponse = gson.fromJson(response, PostCallResponse.class);
            return postCallResponse.getData().getCallId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
