package com.workgig.pro.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.workgig.pro.R;

import java.util.ArrayList;


/**
 * Created by murashid on 20/5/18.
 */
public class JobImageListAdapter extends RecyclerView.Adapter<JobImageListAdapter.ViewHolder> {

    private ArrayList<String> imageUrls;
    private Activity mAcitvity;

    public JobImageListAdapter(Activity mAcitvity, ArrayList<String> imageUrls)
    {
       this.mAcitvity = mAcitvity;
       this.imageUrls = imageUrls;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView ivJobPhoto;
        public ViewHolder(View itemView) {
            super(itemView);
            ivJobPhoto = (ImageView) itemView.findViewById(R.id.ivJobPhoto);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_image_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if(!imageUrls.get(position).equals(""))
        {
            Glide.with(mAcitvity).setDefaultRequestOptions(new RequestOptions() .error(R.drawable.vector_app_logo_profile)
                    .placeholder(R.drawable.vector_app_logo_profile))
                    .load(imageUrls.get(position))
                    .into(holder.ivJobPhoto);
        }
        else
        {
            Glide.with(mAcitvity).setDefaultRequestOptions(new RequestOptions())
                    .load(R.drawable.vector_app_logo_profile)
                    .into(holder.ivJobPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return imageUrls.size();
    }

}
