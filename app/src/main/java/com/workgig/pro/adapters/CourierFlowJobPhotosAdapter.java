package com.workgig.pro.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.workgig.pro.R;

import java.util.ArrayList;

public class CourierFlowJobPhotosAdapter  extends RecyclerView.Adapter{

    Context context;
    private ArrayList<String> imageSource;

    public CourierFlowJobPhotosAdapter(Context context, ArrayList<String> pickupImages) {
        this.context = context;
        this.imageSource = pickupImages;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.job_photos_item,viewGroup,false);
        return new JobPhotosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        Glide.with(context)
                .load(imageSource.get(i))
                .into( ((JobPhotosViewHolder)viewHolder).iv_jobImage);
    }

    @Override
    public int getItemCount() {
        return imageSource.size();
    }

    public class JobPhotosViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_jobImage;
        public JobPhotosViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_jobImage = itemView.findViewById(R.id.iv_jobImage);
        }
    }
}
