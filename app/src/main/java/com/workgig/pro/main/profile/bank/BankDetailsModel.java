package com.workgig.pro.main.profile.bank;

import android.util.Log;


import com.google.gson.Gson;
import com.workgig.pro.pojo.profile.bank.BankList;
import com.workgig.pro.pojo.profile.bank.LegalEntity;
import com.workgig.pro.pojo.profile.bank.StripeDetailsPojo;
import com.workgig.pro.utility.OkHttp3ConnectionStatusCode;
import com.workgig.pro.utility.ServiceUrl;
import com.workgig.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankDetailsModel</h1>
 * BankDetailsModel model for BankDetailsAcitivity
 * @see BankDetailsActivity
 */

public class BankDetailsModel {
    private static final String TAG = "SupportFragModel";
    private BankDetailsModelImplement modelImplement;

    BankDetailsModel(BankDetailsModelImplement modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * mehthod for calling api for getting the bank details
     * @param token sessionToken
     */
    void fetchData(String token) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_CONNECT_ACCOUNT_STRIPE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccessRaiseTicket: "+statusCode+"\n"+result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS))
                        {
                            JSONObject data = jsonObject.getJSONObject("data");
                            if (data.has("legal_entity")) {
                                StripeDetailsPojo stripeDetailsPojo = new Gson().fromJson(data.toString(), StripeDetailsPojo.class);
                                modelImplement.onSuccess(stripeDetailsPojo.getLegal_entity(), stripeDetailsPojo.getExternal_accounts().getData());
                            }
                            else
                            {
                                modelImplement.onFailure(jsonObject.getString("message"));
                            }
                        }
                        else if(statusCode.equals(VariableConstant.RESPONSE_CODE_NO_STRIPE_FOUND) || statusCode.equals(VariableConstant.RESPONSE_CODE_NO_ACCOUNT_FOUND) || statusCode.equals(VariableConstant.RESPONSE_CODE_NOT_FOUND))
                        {
                            modelImplement.noStipeAccount(jsonObject.getString("message"));
                        }
                        else
                        {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (JSONException e) {
                    modelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    /**
     * BankDetailsModelImplement interface for Presenter implementation
     */
    interface BankDetailsModelImplement {
        void onFailure(String failureMsg);
        void onFailure();
        void onSuccess(LegalEntity legalEntity, ArrayList<BankList> bankLists);
        void noStipeAccount(String msg);
    }
}

