package com.workgig.pro.main.profile.profileeedit;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.workgig.pro.pojo.profile.ProfileData;
import com.workgig.pro.pojo.profile.ProfilePojo;
import com.workgig.pro.utility.OkHttp3ConnectionStatusCode;
import com.workgig.pro.utility.RefreshToken;
import com.workgig.pro.utility.ServiceUrl;
import com.workgig.pro.utility.UploadFileAmazonS3;
import com.workgig.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by murashid on 10-Sep-17.
 * <h1>ProfileEditModel</h1>
 * ProfileEditModel model for ProfileEditActivity
 *
 * @see ProfileEditActivity
 */

public class ProfileEditModel {
    private static final String TAG = "ProfileEditModel";
    private ProfileEditImple modelImplement;

    ProfileEditModel(ProfileEditImple modelImplement) {
        this.modelImplement = modelImplement;
    }

    /**
     * method for calling api for geting the profile details
     *
     * @param sessionToken session Token
     */
    void getProfile(final String sessionToken) {
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.GET, new JSONObject()
                , new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
                    @Override
                    public void onSuccess(String statusCode, String result) {
                        try {
                            if (result != null) {
                                Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                                Gson gson = new Gson();
                                JSONObject jsonObject = new JSONObject(result);
                                if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                                    ProfilePojo profilePojo = gson.fromJson(result, ProfilePojo.class);
                                    modelImplement.onSuccessProfileData(profilePojo.getData());
                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_TOKEN_EXPIRE)) {
                                    RefreshToken.onRefreshToken(jsonObject.getString("data"), new RefreshToken.RefreshTokenImple() {
                                        @Override
                                        public void onSuccessRefreshToken(String newToken) {
                                            getProfile(newToken);
                                            modelImplement.onNewToken(newToken);
                                        }

                                        @Override
                                        public void onFailureRefreshToken() {
                                            modelImplement.onFailure();
                                        }

                                        @Override
                                        public void sessionExpired(String msg) {
                                            modelImplement.sessionExpired(msg);
                                        }
                                    });
                                } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN)) {
                                    modelImplement.sessionExpired(jsonObject.getString("message"));
                                } else {
                                    modelImplement.onFailure(jsonObject.getString("message"));
                                }
                            } else {
                                modelImplement.onFailure();
                            }
                        } catch (Exception e) {
                            modelImplement.onFailure();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        modelImplement.onFailure();
                    }
                });
    }

    /**
     * method for calling api for update the profile details and check the local validation
     *
     * @param sessionToken session token
     * @param jsonObject   required field in jsonObject
     */
    void updateProfile(String sessionToken, JSONObject jsonObject) {
        try {
            if (jsonObject.getString("firstName").equals("")) {
                modelImplement.onFirstNameError();
                return;
            } else if (jsonObject.getString("lastName").equals("")) {
                modelImplement.onLastNameError();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.PROFILE, OkHttp3ConnectionStatusCode.Request_type.PATCH, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);

                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            modelImplement.onSuccessUpdateProfile(jsonObject.getString("message"));
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (Exception e) {
                    modelImplement.onFailure();
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }


    /**
     * method for uploading imag in amazon
     *
     * @param mFileTemp file which has been to upload in server
     */
    void amazonUpload( File mFileTemp) {

        Log.d(TAG, "amzonUpload: " + mFileTemp.getPath());
        new UploadFileToServer().execute(mFileTemp.getPath());
    }

    /**
     * method for logout
     *
     * @param sessionToken session Token
     */
    void logout(String sessionToken) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userType", VariableConstant.USER_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        OkHttp3ConnectionStatusCode.doOkHttp3Connection(sessionToken, ServiceUrl.LOGOUT, OkHttp3ConnectionStatusCode.Request_type.POST, new JSONObject(), new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode, String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: " + statusCode + "\n" + result);
                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            modelImplement.sessionExpired(jsonObject.getString("message"));
                        } else if (statusCode.equals(VariableConstant.RESPONSE_CODE_INVALID_TOKEN)) {
                            modelImplement.sessionExpired("");
                        } else {
                            modelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        modelImplement.onFailure();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    modelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                modelImplement.onFailure();
            }
        });
    }

    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.IMAGE_UPLOAD_ON_SERVER)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: " + responseCode);
                Log.d(TAG, "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                    String image = jsonObject.getString("data");
                    modelImplement.onSuccessImageUpload(image);

                } else {

                    //   Toast.makeText(this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();

                // Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }

    }

    /**
     * ProfileEditImple interface for ProfileEdit implementation
     */
    interface ProfileEditImple {
        void onFailure(String failureMsg);

        void onFailure();

        void onSuccessUpdateProfile(String msg);

        void onSuccessProfileData(ProfileData profileData);

        void onSuccessImageUpload(String imgUrl);

        void onNewToken(String newToken);

        void sessionExpired(String msg);

        void onFirstNameError();

        void onLastNameError();
    }
}
