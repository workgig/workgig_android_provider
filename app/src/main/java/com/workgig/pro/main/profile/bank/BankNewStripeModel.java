package com.workgig.pro.main.profile.bank;

import android.os.AsyncTask;
import android.util.Log;
import android.util.Patterns;


import com.workgig.pro.utility.OkHttp3ConnectionStatusCode;
import com.workgig.pro.utility.ServiceUrl;
import com.workgig.pro.utility.UploadFileAmazonS3;
import com.workgig.pro.utility.VariableConstant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by murashid on 25-Aug-17.
 * <h1>BankNewStripeModel</h1>
 * BankNewStripeModel model for BankNewStripeActivity
 * @see BankNewStripeActivity
 */

public class BankNewStripeModel {

    private static final String TAG = "BankNewStripeModel";
    private BankNewStripModelImplement bankNewStripModelImplement;
    private String imageUrl = "";
    private JSONObject jsonObject;
    private String token;

    BankNewStripeModel(BankNewStripModelImplement bankNewStripModelImplement) {
        this.bankNewStripModelImplement = bankNewStripModelImplement;
    }

    /**
     * execute the getIpAddress() class
     */
    void fetchIp() {
        new getIpAddress().execute();
    }

    /**
     * Asyntask for getting the ip address of the mobile connect network
     */
    private class getIpAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String[] params) {
            // do above Server call here
            try {
                return InetAddress.getLocalHost().toString();
            } catch (UnknownHostException e) {
                e.printStackTrace();
                return "007";
            }
        }

        @Override
        protected void onPostExecute(String message) {
            Log.d(TAG, "onPostExecute: "+message);
            bankNewStripModelImplement.ipAddress(message);
        }
    }


    /**
     * method for add bank account
     * @param token session token
     * @param jsonObject required field in json object
     * @param isPictureTaken boolen for picture is taken or not
     * @param amazonS3 object of UploadFileAmazonS3 class
     * @param mFileTemp file that need to upload in amazon
     */
    void addBankAccount(String token, JSONObject jsonObject, boolean isPictureTaken, UploadFileAmazonS3 amazonS3, File mFileTemp) {

            if(!isPictureTaken)
            {
                bankNewStripModelImplement.onErrorImageUpload();
                return;
            }
            amazonUpload(mFileTemp);
            this.jsonObject = jsonObject;
            this.token = token;
    }

    /**
     * mehtod for uploading image in server
     * @param mFileTemp file that need to upload in server
     */
    private void amazonUpload(File mFileTemp)
    {

        Log.i(TAG, "amazonUpload: "+mFileTemp.getPath());
        new UploadFileToServer().execute(mFileTemp.getPath());


    }

    private class UploadFileToServer extends AsyncTask<String, Integer, String[]> {

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

        }

        @Override
        protected String[] doInBackground(String... params) {
            String result = "";
            String responseCode = "";
            String responseWithCode[] = new String[]{result, responseCode};

            try {
                OkHttpClient.Builder builder = new OkHttpClient.Builder();
                builder.connectTimeout(20, TimeUnit.SECONDS);
                builder.readTimeout(20, TimeUnit.SECONDS);
                builder.writeTimeout(20, TimeUnit.SECONDS);
                OkHttpClient httpClient = builder.build();
                httpClient.readTimeoutMillis();

                RequestBody requestBody = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("photo", "Filename.png",
                                RequestBody.create(MediaType.parse("*/*"), new File(params[0])))
                        .build();

                Request request = new Request.Builder()
                        .url(ServiceUrl.IMAGE_UPLOAD_ON_SERVER)
                        .post(requestBody)
                        .build();

                Response response = httpClient.newCall(request).execute();
                result = response.body().string();
                responseCode = String.valueOf(response.code());
                responseWithCode[0] = responseCode;
                responseWithCode[1] = result;

                Log.d(TAG, "doInBackground: " + responseCode);
                Log.d(TAG, "doInBackground: " + result);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseWithCode;
        }

        @Override
        protected void onPostExecute(String[] result) {
            try {
                if (result[0].equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                    JSONObject jsonObject = new JSONObject(result[1]);
                   imageUrl = jsonObject.getString("data");
                   onImageUpload();

                } else {

                    //   Toast.makeText(this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();

                // Toast.makeText(ChattingActivity.this, getString(R.string.uploadImageError), Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(result);
        }

    }

    private void onImageUpload(){
        try {

            jsonObject.put("document", imageUrl);

            Log.d(TAG, "addBankAccount: "+jsonObject);

            if(jsonObject.getString("first_name").equals(""))
            {
                bankNewStripModelImplement.onErrorFirstName();
                return;
            }
            else if(jsonObject.getString("last_name").equals(""))
            {
                bankNewStripModelImplement.onErrorLastName();
                return;
            }
            else if (!Patterns.EMAIL_ADDRESS.matcher(jsonObject.getString("email")).matches()) {
                bankNewStripModelImplement.onErrorEmail();
                return;
            }
            else if(jsonObject.getString("year").equals(""))
            {
                bankNewStripModelImplement.onErrorDob();
                return;
            }
            else if(jsonObject.getString("personal_id_number").equals(""))
            {
                bankNewStripModelImplement.onErrorSSN();
                return;
            }
            else if(jsonObject.getString("line1").equals(""))
            {
                bankNewStripModelImplement.onErrorAddress();
                return;
            }
            else if(jsonObject.getString("city").equals(""))
            {
                bankNewStripModelImplement.onErrorCity();
                return;
            }
            else if(jsonObject.getString("state").equals(""))
            {
                bankNewStripModelImplement.onErrorState();
                return;
            }
            else if(jsonObject.getString("country").equals(""))
            {
                bankNewStripModelImplement.onErrorCountry();
                return;
            }

            else if(jsonObject.getString("postal_code").equals(""))
            {
                bankNewStripModelImplement.onErrorZipcode();
                return;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        OkHttp3ConnectionStatusCode.doOkHttp3Connection(token, ServiceUrl.BANK_CONNECT_ACCOUNT_STRIPE, OkHttp3ConnectionStatusCode.Request_type.POST, jsonObject, new OkHttp3ConnectionStatusCode.OkHttp3RequestCallback() {
            @Override
            public void onSuccess(String statusCode,String result) {
                try {
                    if (result != null) {
                        Log.d(TAG, "onSuccess: "+statusCode+"\n"+result);

                        JSONObject jsonObject = new JSONObject(result);
                        if (statusCode.equals(VariableConstant.RESPONSE_CODE_SUCCESS)) {
                            bankNewStripModelImplement.onSuccess(jsonObject.getString("message"));
                        } else {
                            bankNewStripModelImplement.onFailure(jsonObject.getString("message"));
                        }
                    } else {
                        bankNewStripModelImplement.onFailure();
                    }
                } catch (Exception e) {
                    bankNewStripModelImplement.onFailure();
                }
            }

            @Override
            public void onError(String error) {
                bankNewStripModelImplement.onFailure();
            }
        });
    }

    /**
     * interface for  presenter implementation
     */
    interface BankNewStripModelImplement {
        void onFailure();
        void onFailure(String msg);
        void onSuccess(String failureMsg);
        void ipAddress(String ip);

        void onErrorImageUpload();
        void onErrorFirstName();
        void onErrorLastName();
        void onErrorEmail();
        void onErrorDob();
        void onErrorSSN();
        void onErrorAddress();
        void onErrorCity();
        void onErrorState();
        void onErrorCountry();
        void onErrorZipcode();
    }

}
