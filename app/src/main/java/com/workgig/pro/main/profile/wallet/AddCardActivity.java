package com.workgig.pro.main.profile.wallet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.workgig.pro.R;
import com.workgig.pro.utility.AppController;
import com.workgig.pro.utility.SessionManager;
import com.workgig.pro.utility.Utility;
import com.workgig.pro.utility.VariableConstant;
import com.stripe.android.model.Card;
import com.stripe.android.view.CardMultilineWidget;

import java.util.List;

import eu.janmuller.android.simplecropimage.Util;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;


public class AddCardActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks, AddCardPresenter.View {

    private static final int MY_SCAN_REQUEST_CODE = 999;
    private ProgressDialog progressDialog;
    private AddCardPresenter presenter;
    private SessionManager sessionManager;

    private CardMultilineWidget card_input_widget_card;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        init();
    }

    /**
     * <h2>initVariablesAndHelperClasses</h2>
     * <p>
     * method to initialize variables and other helper
     * classes
     * </p>
     */
    private void init() {
        sessionManager = SessionManager.getSessionManager(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.setCancelable(false);
        presenter = new AddCardPresenter(this);

        Typeface fontRegular = Utility.getFontRegular(this);
        Typeface fontMedium = Utility.getFontMedium(this);
        Typeface fontBold = Utility.getFontBold(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.vector_color_primary_back_button);
        }
        TextView tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.addcard));
        tvTitle.setTypeface(fontBold);

        TextView tvScanCard = findViewById(R.id.tvScanCard);
        tvScanCard.setTypeface(fontMedium);
        tvScanCard.setOnClickListener(this);

        TextView tvDoneCard = findViewById(R.id.tvDoneCard);
        tvDoneCard.setTypeface(fontMedium);
        tvDoneCard.setOnClickListener(this);

        card_input_widget_card = findViewById(R.id.card_input_widget_card);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvScanCard:
                Utility.hideKeyboad(this);
                scanCard();

               /* if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
                    scanCard();
                } else {
                    EasyPermissions.requestPermissions(this, getString(R.string.read_storage_and_camera_state_permission_message),
                            1000, VariableConstant.STORAGE_CAMERA_PERMISSION);
                }*/
                break;

            case R.id.tvDoneCard:
                Utility.hideKeyboad(this);
                presenter.getStripeToken(this, card_input_widget_card.getCard(), sessionManager.getStripeKey(),
                        AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getEmail());
                break;
            default:
                break;
        }
    }

    /**
     *
     */
    private void scanCard() {


        Intent scanIntent = new Intent(AddCardActivity.this, CardIOActivity.class);
        // customize these values to suit your needs.
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true); // default: false
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true); // default: false
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {


            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                scanCardResult(scanResult);
            }
            /*String resultDisplayStr;
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
                resultDisplayStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";
               // Log.d(TAG, "Card Number " + resultDisplayStr);
                // Do something with the raw number, e.g.:
                // myService.setCardNumber( scanResult.cardNumber );
                if (scanResult.isExpiryValid()) {
                    resultDisplayStr += "Expiration Date: " + scanResult.expiryMonth + "/" + scanResult.expiryYear + "\n";
                }
                if (scanResult.cvv != null) {
                    // Never log or display a CVV
                    resultDisplayStr += "CVV has " + scanResult.cvv.length() + " digits.\n";
                }
                if (scanResult.postalCode != null) {
                    resultDisplayStr += "Postal Code: " + scanResult.postalCode + "\n";
                }
            }*/
            else {
                Toast.makeText(AddCardActivity.this, "Scan was canceled.", Toast.LENGTH_SHORT).show();
            }

            // Log.d(TAG, "onActivityResult: ");
            // do something with resultDisplayStr, maybe display it in a textView
            // resultTextView.setText(resultDisplayStr);
        }


    }

    /**
     * <h2>scanCardResult</h2>
     * <p>
     * method to get values from the scanned card
     * </p>
     * <p>
     * //     * @param scanResult
     */
    private void scanCardResult(CreditCard scanResult) {

        // Never log a raw card number. Avoid displaying it, but if necessary use getFormattedCardNumber()
        //resultStr = "Card Number: " + scanResult.getRedactedCardNumber() + "\n";

       Utility.printLog("scanCardResult: ", "cardNumber=" + scanResult.cardNumber + " expiryMonth=" + scanResult.expiryMonth
                + " expiryYear=" + scanResult.expiryMonth + " cvv=" + scanResult.cvv);
        try {

            Card card = new Card(scanResult.cardNumber, scanResult.expiryMonth, scanResult.expiryYear, scanResult.cvv);
            Utility.hideKeyboad(this);
            presenter.getStripeToken(this, card, sessionManager.getStripeKey(),
                    AppController.getInstance().getAccountManagerHelper().getAuthToken(sessionManager.getEmail()), sessionManager.getEmail());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            closeActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        closeActivity();
    }

    private void closeActivity() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }
    }

    @Override
    public void startProgressBar() {
        progressDialog.show();
    }

    @Override
    public void stopProgressBar() {
        progressDialog.dismiss();
    }

    @Override
    public void onFailure(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, getString(R.string.unableToAddCard), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        VariableConstant.IS_CARD_UPDATED = true;
        closeActivity();
    }

    @Override
    public void invalidCard() {
        Toast.makeText(this, getString(R.string.plsEnterValidCard), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNewToken(String token) {
        AppController.getInstance().getAccountManagerHelper().setAuthToken(sessionManager.getEmail(),
                sessionManager.getPassword(), token);
    }

    @Override
    public void sessionExpired(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Utility.logoutSessionExiperd(sessionManager, this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
       /* if (EasyPermissions.hasPermissions(this, VariableConstant.STORAGE_CAMERA_PERMISSION)) {
            scanCard();
        }*/
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }
}
