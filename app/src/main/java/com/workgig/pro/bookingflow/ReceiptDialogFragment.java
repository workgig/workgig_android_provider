package com.workgig.pro.bookingflow;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.workgig.pro.R;
import com.workgig.pro.adapters.CourierFlowJobPhotosAdapter;
import com.workgig.pro.pojo.booking.Booking;
import com.workgig.pro.pojo.booking.ServiceItem;
import com.workgig.pro.pojo.history.Accounting;
import com.workgig.pro.pojo.history.AdditionalService;
import com.workgig.pro.utility.SessionManager;
import com.workgig.pro.utility.Utility;

import java.util.ArrayList;

/**
 * Created by murashid on 08-Aug-18.
 */

public class ReceiptDialogFragment extends DialogFragment {

    private static final String ARG_PARAM1 = "param1";
    private Booking booking;
    private ArrayList<String> pickupImages, dropImages;

    public static ReceiptDialogFragment newInstance(Booking booking) {
        ReceiptDialogFragment fragment = new ReceiptDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, booking);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            booking = (Booking) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogThemeBottom;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View rootView = inflater.inflate(R.layout.dialog_receipt, null);
        builder.setView(rootView);

        initView(rootView);
        return builder.create();
    }

    private void initView(View rootView) {
        Accounting accounting = booking.getAccounting();

        Typeface fontMedium = Utility.getFontMedium(getActivity());
        Typeface fontRegular = Utility.getFontRegular(getActivity());
        SessionManager sessionManager = SessionManager.getSessionManager(getActivity());

        TextView tvReceipt = rootView.findViewById(R.id.tvReceipt);
        TextView tvJobLocationLabel = rootView.findViewById(R.id.tvJobLocationLabel);
        TextView tvJobLocation = rootView.findViewById(R.id.tvJobLocation);
        TextView tvPaymentBreakDownLabel = rootView.findViewById(R.id.tvPaymentBreakDownLabel);
        TextView tvCancelFeeLabel = rootView.findViewById(R.id.tvCancelFeeLabel);
        TextView tvCancelFee = rootView.findViewById(R.id.tvCancelFee);
        TextView tvServiceChargeLabel = rootView.findViewById(R.id.tvServiceChargeLabel);
        TextView tvServiceCharge = rootView.findViewById(R.id.tvServiceCharge);
        TextView tvNoMiscAppliedLabel = rootView.findViewById(R.id.tvNoMiscAppliedLabel);
        TextView tvNoMiscApplied = rootView.findViewById(R.id.tvNoMiscApplied);

        TextView tvTravelFeeLabel = rootView.findViewById(R.id.tvTravelFeeLabel);
        TextView tvTravelFee = rootView.findViewById(R.id.tvTravelFee);
        TextView tvVisitFeeLabel = rootView.findViewById(R.id.tvVisitFeeLabel);
        TextView tvDiscountLabel = rootView.findViewById(R.id.tvDiscountLabel);
        TextView tvVisitFee = rootView.findViewById(R.id.tvVisitFee);
        TextView tvDiscount = rootView.findViewById(R.id.tvDiscount);
        TextView tvLastDueLabel = rootView.findViewById(R.id.tvLastDueLabel);
        TextView tvLastDue = rootView.findViewById(R.id.tvLastDue);

        TextView tvTotalLabel = rootView.findViewById(R.id.tvTotalLabel);
        TextView tvTotal = rootView.findViewById(R.id.tvTotal);
        TextView tvPickUpPhotos = rootView.findViewById(R.id.tvPickUpPhotos);
        TextView tvDropPhotos = rootView.findViewById(R.id.tvDropPhotos);
        RecyclerView ivPickUpPhotos = rootView.findViewById(R.id.ivPickUpPhotos);
        RecyclerView ivDropPhotos = rootView.findViewById(R.id.ivDropPhotos);
        LinearLayout lljobPhotos = rootView.findViewById(R.id.lljobPhotos);


        TextView tvPaymentMethodLabel = rootView.findViewById(R.id.tvPaymentMethodLabel);

        TextView tvPaymentMethod = rootView.findViewById(R.id.tvPaymentMethod);
        TextView tvPaymentCardCash = rootView.findViewById(R.id.tvPaymentCardCash);
        TextView tvPaymentMethodWallet = rootView.findViewById(R.id.tvPaymentMethodWallet);
        TextView tvPaymentWallet = rootView.findViewById(R.id.tvPaymentWallet);
        RelativeLayout rlWalletPaymentDetails = rootView.findViewById(R.id.rlWalletPaymentDetails);
        LinearLayout llService = rootView.findViewById(R.id.llService);

        TextView tvSignatureHeader = rootView.findViewById(R.id.tvSignatureHeader);
        ImageView ivSignature = rootView.findViewById(R.id.ivSignature);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        rootView.findViewById(R.id.ivCloseButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        tvReceipt.setTypeface(Utility.getFontBold(getActivity()));

        tvJobLocationLabel.setTypeface(fontMedium);
        tvJobLocation.setTypeface(fontRegular);
        tvTotalLabel.setTypeface(fontMedium);
        tvTotal.setTypeface(fontMedium);
        tvPaymentMethodLabel.setTypeface(fontMedium);
        tvPaymentMethod.setTypeface(fontMedium);
        tvPaymentCardCash.setTypeface(fontMedium);
        tvPaymentMethodWallet.setTypeface(fontMedium);
        tvPaymentWallet.setTypeface(fontMedium);
        tvSignatureHeader.setTypeface(fontMedium);

        tvTravelFeeLabel.setTypeface(fontRegular);
        tvTravelFee.setTypeface(fontRegular);
        tvVisitFeeLabel.setTypeface(fontRegular);
        tvDiscountLabel.setTypeface(fontRegular);
        tvVisitFee.setTypeface(fontRegular);
        tvDiscount.setTypeface(fontRegular);
        tvLastDueLabel.setTypeface(fontRegular);
        tvLastDue.setTypeface(fontRegular);
        tvPaymentBreakDownLabel.setTypeface(fontMedium);
        tvServiceChargeLabel.setTypeface(fontRegular);
        tvServiceCharge.setTypeface(fontRegular);
        tvNoMiscAppliedLabel.setTypeface(fontRegular);
        tvNoMiscApplied.setTypeface(fontRegular);
        tvCancelFeeLabel.setTypeface(fontRegular);
        tvCancelFee.setTypeface(fontRegular);


        pickupImages = booking.getPickupImages();
        dropImages = booking.getDropImages();

        if(pickupImages!=null && pickupImages.size()>0){
            ivPickUpPhotos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            ivPickUpPhotos.setAdapter(new CourierFlowJobPhotosAdapter(getActivity() , pickupImages));
        }
        else
            tvPickUpPhotos.setVisibility(View.GONE);

        if(dropImages!=null && dropImages.size()>0){
            ivDropPhotos.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            ivDropPhotos.setAdapter(new CourierFlowJobPhotosAdapter(getActivity(),dropImages));
        }
        else
            tvDropPhotos.setVisibility(View.GONE);

        if (booking.getCallType().equals("2")) {
            tvJobLocation.setText(booking.getAddLine1() + " " + booking.getAddLine2());
        } else {
            tvJobLocation.setVisibility(View.GONE);
            tvJobLocationLabel.setVisibility(View.GONE);
            rootView.findViewById(R.id.jobLabelView).setVisibility(View.GONE);
        }
        tvTotal.setText(Utility.getPrice(accounting.getTotal(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));

        if (accounting.getTravelFee() != null && !accounting.getTravelFee().equals("0") && !accounting.getTravelFee().equals("0.00")) {
            rootView.findViewById(R.id.rlTravelFee).setVisibility(View.VISIBLE);
            tvTravelFee.setText(Utility.getPrice(accounting.getTravelFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getVisitFee() != null && !accounting.getVisitFee().equals("0") && !accounting.getVisitFee().equals("0.00")) {
            rootView.findViewById(R.id.rlVisitFee).setVisibility(View.VISIBLE);
            tvVisitFee.setText(Utility.getPrice(accounting.getVisitFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getDiscount() != null && !accounting.getDiscount().equals("0") && !accounting.getDiscount().equals("0.00")) {
            rootView.findViewById(R.id.rldiscount).setVisibility(View.VISIBLE);
            tvDiscount.setText(Utility.getPrice(accounting.getDiscount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }
        if (accounting.getLastDues() != null && !accounting.getLastDues().equals("0") && !accounting.getLastDues().equals("0.00")) {
            rootView.findViewById(R.id.rlLastDue).setVisibility(View.VISIBLE);
            tvLastDue.setText(Utility.getPrice(accounting.getLastDues(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (accounting.getPaymentMethod().equals("1")) {
            tvPaymentMethod.setText(getString(R.string.cash));
            tvPaymentMethod.setCompoundDrawablesWithIntrinsicBounds(R.drawable.vector_cash, 0, 0, 0);
        } else {
            tvPaymentMethod.setText(getString(R.string.card));
        }

        /*if(accounting.getServiceFee() != null && !accounting.getServiceFee().equals("") && !accounting.getServiceFee().equals("0"))
        {
            //rootView.findViewById(R.id.rlServiceCharge).setVisibility(View.VISIBLE);
            tvServiceCharge.setText(Utility.getPrice(accounting.getServiceFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }*/

        tvPaymentCardCash.setText(Utility.getPrice(accounting.getRemainingAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        tvPaymentCardCash.setVisibility(View.GONE);

        if (accounting.getPaidByWallet() != null && accounting.getPaidByWallet().equals("1")) {
            rlWalletPaymentDetails.setVisibility(View.VISIBLE);
            tvPaymentWallet.setText(Utility.getPrice(accounting.getCaptureAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (!booking.getBookingModel().equals("3")) {
            if (booking.getServiceType().equals("1")) {
                if (booking.getCartData() != null) {
                    for (ServiceItem serviceItem : booking.getCartData()) {
                        View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                        TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                        TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                        tvServiceFeeLabel.setTypeface(fontRegular);
                        tvServiceFee.setTypeface(fontRegular);
                        tvServiceFeeLabel.setText(serviceItem.getServiceName() + " X " + serviceItem.getQuntity());
                        tvServiceFee.setText(Utility.getPrice(serviceItem.getAmount(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                        llService.addView(serviceView);
                    }
                }
            } else {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
//                tvServiceFeeLabel.setText(Utility.converSecondToHourMinute(accounting.getTotalActualJobTimeMinutes()));
                tvServiceFeeLabel.setText(getResources().getString(R.string.consultingfee));
                tvServiceFee.setText(Utility.getPrice(accounting.getTotalActualHourFee(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        } else {
            View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
            TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
            TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
            tvServiceFeeLabel.setTypeface(fontRegular);
            tvServiceFee.setTypeface(fontRegular);
            tvServiceFeeLabel.setText(getString(R.string.jobPay));
            tvServiceFee.setText(Utility.getPrice(accounting.getProviderEarning(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
            llService.addView(serviceView);
        }

        if (booking.getAdditionalService() != null && booking.getAdditionalService().size() > 0) {
            for (AdditionalService serviceItem : booking.getAdditionalService()) {
                View serviceView = inflater.inflate(R.layout.single_row_service_with_fees_in_history, null);
                TextView tvServiceFeeLabel = serviceView.findViewById(R.id.tvServiceFeeLabel);
                TextView tvServiceFee = serviceView.findViewById(R.id.tvServiceFee);
                tvServiceFeeLabel.setTypeface(fontRegular);
                tvServiceFee.setTypeface(fontRegular);
                tvServiceFeeLabel.setText(serviceItem.getServiceName());
                tvServiceFee.setText(Utility.getPrice(serviceItem.getPrice(), sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
                llService.addView(serviceView);
            }
        } else {
            rootView.findViewById(R.id.rlNoMiscApplied).setVisibility(View.VISIBLE);
            tvNoMiscApplied.setText(Utility.getPrice("0", sessionManager.getCurrencySymbol(), sessionManager.getCurrencyAbbrevation()));
        }

        if (booking.getSignatureUrl() != null && !booking.getSignatureUrl().equals("")) {
            Glide.with(this).setDefaultRequestOptions(new RequestOptions())
                   . load(booking.getSignatureUrl())
                    .into(ivSignature);
        } else {
            rootView.findViewById(R.id.llSignature).setVisibility(View.GONE);
        }
    }
}
